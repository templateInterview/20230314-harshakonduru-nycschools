## Name
NYC High Schools

## Description
Display list of high schools and their details from Socrata Open Data API.

## Installation
To install the application in an android or emulator device, the minimum requirement is android studio IDE.
Steps to install the application:
1. Clone the repository.
2. Run the project using android studio targeting your desired device.