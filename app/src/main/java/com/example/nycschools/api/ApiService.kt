package com.example.nycschools.api

import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface to maintain the methods for the api calls.
 */
interface ApiService {

    /**
     * Get list of schools for the provided limit and offset.
     * @param limit number of items needed from the api call.
     * @param offset start position of the list from the total set to receive the data.
     */

    @GET("resource/s3k6-pzi2.json")
    suspend fun getListOfSchools(
        @Query("\$limit") limit: Int,
        @Query("\$offset") offset: Int
    ): Response<List<School>>

    /**
     * To get the school details using the dbn number.
     * @param dbNumber db number of the school
     */
    @GET("resource/f9bf-2cp4.json")
    suspend fun getSchoolDetails(
        @Query("dbn") dbNumber: String
    ): Response<List<SchoolDetails>>
}