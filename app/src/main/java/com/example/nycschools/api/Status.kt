package com.example.nycschools.api

/**
 * Created by Harsha Konduru
 */
enum class Status {
    /**
     * Waiting for response.
     */
    Waiting,

    /**
     * Obtained response successfully.
     */
    SUCCESS,

    /**
     * Failed to get response.
     */
    ERROR
}