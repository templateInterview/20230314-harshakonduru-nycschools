package com.example.nycschools.repository

import com.example.nycschools.api.Resource
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolDetails

/**
 * Created by Harsha Konduru
 * Consists of list of Api calls for the NYC Schools feature.
 */
interface SchoolRepository {

    suspend fun getListOfSchools(limit: Int, offset: Int): Resource<List<School>?>

    suspend fun getSchoolDetails(dbn: String): Resource<List<SchoolDetails>?>
}