package com.example.nycschools.schooldetails;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nycschools.api.RetrofitProvider;
import com.example.nycschools.databinding.FragmentSchoolDetailsBinding;
import com.example.nycschools.models.School;
import com.example.nycschools.repository.SchoolRepository;
import com.example.nycschools.repository.SchoolRepositoryImpl;

import kotlinx.coroutines.Dispatchers;

/**
 * Provide List of Schools received from the data set.
 */
public class SchoolDetailsFragment extends Fragment {

    private FragmentSchoolDetailsBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentSchoolDetailsBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        School school = SchoolDetailsFragmentArgs.fromBundle(getArguments()).getSchool();

        if (school == null) {
            binding.errorMessage.setVisibility(View.VISIBLE);
            return;
        }

        // Dagger and ViewModelFactory can be used.
        SchoolRepository repository = new SchoolRepositoryImpl(RetrofitProvider.getInstance().getApiService());
        SchoolDetailsViewModel viewModel = new SchoolDetailsViewModel(repository, Dispatchers.getIO());
        viewModel.getDetails(school);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setViewModel(viewModel);
    }
}