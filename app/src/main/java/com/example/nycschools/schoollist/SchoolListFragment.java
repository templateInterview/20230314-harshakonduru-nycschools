package com.example.nycschools.schoollist;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nycschools.api.RetrofitProvider;
import com.example.nycschools.databinding.FragmentSchoolListBinding;
import com.example.nycschools.repository.SchoolRepository;
import com.example.nycschools.repository.SchoolRepositoryImpl;
import com.example.nycschools.schoollist.SchoolListFragmentDirections.ActionSchoolListFragmentToSchoolDetailsFragment;

import kotlin.Unit;

/**
 * Provide List of Schools received from the data set.
 */
public class SchoolListFragment extends Fragment {

    private FragmentSchoolListBinding binding;
    private SchoolPagedListAdapter adapter;
    private SchoolListViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentSchoolListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Dependency Injection can be used.
        SchoolRepository repository = new SchoolRepositoryImpl(RetrofitProvider.getInstance().getApiService());
        viewModel = new SchoolListViewModel(repository);
        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        initializeRv();
        registerObservers();
    }

    private void initializeRv() {
        adapter = new SchoolPagedListAdapter(school -> {
            ActionSchoolListFragmentToSchoolDetailsFragment action = SchoolListFragmentDirections.actionSchoolListFragmentToSchoolDetailsFragment(school);
            action.setSchool(school);
            Navigation.findNavController(binding.getRoot()).navigate(action);
            return Unit.INSTANCE;
        });
        binding.schoolsList.setAdapter(adapter);
        binding.schoolsList.setHasFixedSize(true);
        binding.schoolsList.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    /**
     * Register observers to observe LiveData from ViewModel
     */
    public void registerObservers() {
        viewModel.getSchoolsList().observe(getViewLifecycleOwner(), schools -> {
            adapter.submitList(schools);
        });
    }
}