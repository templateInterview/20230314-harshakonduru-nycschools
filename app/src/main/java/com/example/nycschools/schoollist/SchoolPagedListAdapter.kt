package com.example.nycschools.schoollist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.R
import com.example.nycschools.databinding.ItemSchoolBinding
import com.example.nycschools.models.School

/**
 * Created by Harsha Konduru.
 */
class SchoolPagedListAdapter(
    private val itemClick: (school: School) -> Unit
) : PagedListAdapter<School, SchoolPagedListAdapter.ItemViewHolder>(diffUtilCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemSchoolBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_school, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, itemClick)
        }
    }

    class ItemViewHolder(private val binding: ItemSchoolBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: School, itemClick: (school: School) -> Unit) {
            with(binding) {
                school = obj
                executePendingBindings()
                itemLayout.setOnClickListener { itemClick.invoke(obj) }
            }
        }
    }

    companion object {
        var diffUtilCallback = object : DiffUtil.ItemCallback<School>() {
            override fun areItemsTheSame(oldItem: School, newItem: School) =
                oldItem.dbn == newItem.dbn

            override fun areContentsTheSame(oldItem: School, newItem: School) = oldItem == newItem

        }
    }
}