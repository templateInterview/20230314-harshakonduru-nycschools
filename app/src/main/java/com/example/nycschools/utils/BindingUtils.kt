package com.example.nycschools.utils

import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter

/**
 * Created by Harsha Konduru.
 */
object BindingUtils {

    /**
     * Set view to visible if the data provided is not empty or null.
     */
    @JvmStatic
    @BindingAdapter("showIfNotNullOrEmpty")
    fun View.showIfNotNullOrEmpty(input: String?) {
        setVisibility(input.isNullOrEmpty().not())
    }

    /**
     * Set view to visible if true. Otherwise view will be gone.
     */
    @JvmStatic
    @BindingAdapter("showIf")
    fun View.setVisibility(isVisible: Boolean?) {
        this.isVisible = isVisible ?: false
    }
}