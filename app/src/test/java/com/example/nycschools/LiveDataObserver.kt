package com.example.nycschools

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

object LiveDataObserver {

    private val TIMEOUT_SEC = 1

    fun <T> observerAndGet(liveData: LiveData<T>): T? {
        val data = arrayOfNulls<Any>(1)
        val latch = CountDownLatch(1)
        val observer: Observer<*> = object : Observer<T> {
            override fun onChanged(t: T) {
                data[0] = t
                latch.countDown()
                liveData.removeObserver(this)
            }
        }
        liveData.observeForever(observer as Observer<in T>)
        try {
            latch.await(TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
        } catch (exception: InterruptedException) {
            return null
        }
        return data[0] as T
    }
}