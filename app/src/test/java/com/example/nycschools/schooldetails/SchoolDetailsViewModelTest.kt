package com.example.nycschools.schooldetails

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.nycschools.LiveDataObserver
import com.example.nycschools.api.Resource
import com.example.nycschools.api.Status
import com.example.nycschools.models.School
import com.example.nycschools.models.SchoolDetails
import com.example.nycschools.repository.SchoolRepository
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verifyOrder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/***
 * Created by Harsha Konduru
 */

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolDetailsViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    val instantExecutionRole = InstantTaskExecutorRule()

    private val mockRepository = mockk<SchoolRepository>()
    private val mockSchool = mockk<School>(relaxed = true) {
        coEvery { dbn } returns "TEST_SCHOOL"
    }
    private lateinit var viewModel: SchoolDetailsViewModel

    private val observer = mockk<Observer<Status>>(relaxed = true)

    @Before
    fun setUp() {
        viewModel = SchoolDetailsViewModel(mockRepository, Dispatchers.IO)
    }


    @Test
    fun `should notify success and no data available if response is empty`() {
        runTest {
            coEvery { mockRepository.getSchoolDetails(any()) } returns Resource.success(listOf())
            viewModel.getDetails(mockSchool)
            viewModel.status.observeForever(observer)
            verifyOrder {
                observer.onChanged(Status.Waiting)
                observer.onChanged(Status.SUCCESS)
            }
            val noDataReceived = LiveDataObserver.observerAndGet(viewModel.noDataReceived)
            Assert.assertTrue(noDataReceived ?: false)
        }
    }

    @Test
    fun `should notify error and response failed to get`() {
        runTest {
            coEvery { mockRepository.getSchoolDetails(any()) } returns Resource.error("")
            viewModel.getDetails(mockSchool)
            viewModel.status.observeForever(observer)
            verifyOrder {
                observer.onChanged(Status.Waiting)
                observer.onChanged(Status.ERROR)
            }
        }
    }

    @Test
    fun `should notify details with status if able to get sat details`() {
        runTest {
            val mockDetail = mockk<SchoolDetails>(relaxed = true)
            coEvery {
                mockRepository.getSchoolDetails(any())
            } returns Resource.success(listOf(mockDetail))
            viewModel.getDetails(mockSchool)
            viewModel.status.observeForever(observer)
            verifyOrder {
                observer.onChanged(Status.Waiting)
                observer.onChanged(Status.SUCCESS)
            }
            val school = LiveDataObserver.observerAndGet(viewModel.schoolDetails)
            Assert.assertNotNull(school)
            Assert.assertEquals(mockSchool, school?.first)
            Assert.assertEquals(mockDetail, school?.second)
        }
    }

    @Test
    fun `should notify error if there is a coroutine exception`() {
        runTest {
            coEvery { mockRepository.getSchoolDetails(any()) } throws IllegalStateException("")
            viewModel.getDetails(mockSchool)
            viewModel.status.observeForever(observer)
            verifyOrder {
                observer.onChanged(Status.Waiting)
                observer.onChanged(Status.ERROR)
            }
        }
    }
}